﻿Shader "Imp/Vertex Colored Diffuse" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
//        CULL off
       
        CGPROGRAM
        #pragma surface surf Lambert
       
        sampler2D _MainTex;
 
        struct Input {
            float2 uv_MainTex;
            float4 color: Color;
        };
        
        void surf (Input IN, inout SurfaceOutput o) {
            o.Albedo = IN.color.rgb;
            o.Alpha = 1.0f;
        }
        ENDCG
    }
    FallBack "Diffuse"
}