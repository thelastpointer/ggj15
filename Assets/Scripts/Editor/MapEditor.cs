using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (Map))]
internal class GenerateTerrainEditor : Editor
{
    Map mapScript;

    public void OnEnable()
    {
        mapScript = (Map) target;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        DrawDefaultInspector();

        if (GUILayout.Button("Generate Map"))
        {
            mapScript.Load();
        }
    }
}