using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (LevelStateManager))]
internal class LevelStateManagerEditor : Editor
{
    LevelStateManager script;

    public void OnEnable()
    {
        script = (LevelStateManager)target;
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        DrawDefaultInspector();

        if (GUILayout.Button("PrevMap"))
        {
            script.LoadPrev();
        }
        if (GUILayout.Button("NextMap"))
        {
            script.LoadNext();
        }
    }
}