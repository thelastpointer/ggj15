﻿using UnityEngine;
using System.Collections;

public class Package : PickupAble
{
    public FriendlyDispenser dispenser;

    public void OnDestroy()
    {
        player.IsCarrying = false;
    }

    public override void OnPickUp()
    {
        dispenser.hasCube = false;
        player.ItemCarried = ItemType.Package;
    }

    public override void OnDropped()
    {
        player.ItemCarried = ItemType.None;
    }
}
