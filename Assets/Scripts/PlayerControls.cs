/*
	PlayerControls.cs
	Controls the player character around the level.
*/
using UnityEngine;
using System.Collections;

public enum ItemType
{
    None,
    Key,
    Package
}

public class PlayerControls : MonoBehaviour
{
	public float MoveSpeed = 5;
	public float SprintSpeed = 50;
	public Transform GFX;
	public Transform Cursor;
	
	CharacterController control;
	public Vector3 lastLook = new Vector3(-1, 0, -1);

    public bool IsCarrying;
    public Vector3 startPosition;
    public ItemType ItemCarried;
    public Transform CarriedObj;

    public GameObject keyPrefab;

	void Awake()
	{
	    control = GetComponent<CharacterController>();
	    startPosition = transform.position;
	}

	void Start()
	{
		StartCoroutine(LevelBeginAnim());
	}
	
	void Update()
	{
	    // Move dude around the level
#if UNITY_EDITOR
		Vector3 input = new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));
#else
		Vector3 input = new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));
#endif
	    input = Quaternion.Euler(0, 135, 0) * input;
		if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			control.SimpleMove(input * SprintSpeed * Time.deltaTime);
		else
			control.SimpleMove(input * MoveSpeed * Time.deltaTime);

		Vector3 lookDir = (input == Vector3.zero ? lastLook : input);
		bool attacking = false;
	    
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
	    // Place cursor - mouse controls only
	    int mask = 0xffff;
	    RaycastHit hit;
	    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50, mask))
	    {
	        Cursor.gameObject.SetActive(true);
	        Cursor.position = hit.point + new Vector3(0, 0.1f, 0);

			if (Input.GetMouseButton(0))
			{
				lookDir = Cursor.position - transform.position;
				lookDir.y = 0;

				attacking = true;
			}
	    }
		else
		    Cursor.gameObject.SetActive(false);
#else
		Vector3 attackDir = new Vector3(Input.GetAxis("Mouse Y"), 0, -Input.GetAxis("Mouse X"));
		if (attackDir != Vector3.zero)
		{
			lookDir = Quaternion.Euler(0, -45, 0) * attackDir;
			attacking = true;
		}
#endif

		// Handle attacks
		if (attacking)
		{
			//...
		}

		// Animate
    	GFX.transform.rotation = Quaternion.LookRotation(lookDir);
		/*if (input != Vector3.zero)
			Anim.CrossFade("run");
		else
			Anim.CrossFade("idle", 0.1f);
		*/
		lastLook = lookDir;
	}
	/*
	#region Console commands for camera

	[TLP.ConsoleCommand]
	static void CameraType(string type)
	{
		if (type.ToLower() == "ortho")
			Camera.main.orthographic = true;
		else if (type.ToLower() == "pers")
			Camera.main.orthographic = false;
		else
			Debug.LogError("Invalid type: use \"ortho\" or \"pers\" as parameter");
	}
	[TLP.ConsoleCommand]
	static void CameraOrthoSize(float size)
	{
		Camera.main.orthographicSize = size;
	}
	[TLP.ConsoleCommand]
	static void CameraFOV(float fov)
	{
		Camera.main.fieldOfView = fov;
	}
	[TLP.ConsoleCommand]
	static void CameraPos(float x, float y, float z)
	{
		Camera.main.transform.localPosition = new Vector3(x, y, z);
	}
	[TLP.ConsoleCommand]
	static void GetCameraPos()
	{
		Debug.Log(Camera.main.transform.localPosition);
	}
	#endregion
	 * */

    public void MoveToStartPosition()
    {
        transform.position = startPosition;
    }

    public void Death()
    {
		Debug.LogError("DEATH");
		StartCoroutine(DeathAnim());
    }

	IEnumerator LevelBeginAnim()
	{
		Debug.Log("FOG::PlayerControls1");
		GlobalFog gf = FindObjectOfType<GlobalFog>();
		if (gf != null)
		{
			float f = 0;
			float from = 3.5f;
			float to = -2.29f;
			while (f < 1)
			{
				f += Time.deltaTime * 1f;

				gf.height = Mathf.SmoothStep(from, to, f);

				yield return null;
			}
			gf.height = to;
		}
	}

	IEnumerator DeathAnim()
	{
		Debug.Log("FOG::PlayerControls2");
		
		GlobalFog gf = FindObjectOfType<GlobalFog>();
		if (gf != null)
		{
			float f = 0;
			float from = -2.29f;
			float to = 3.5f;
			while (f < 1)
			{
				f += Time.deltaTime * 1f;

				gf.height = Mathf.SmoothStep(from, to, f);

				yield return null;
			}
			gf.height = to;
		}

		IsCarrying = false;
		MoveToStartPosition();

		if (CarriedObj != null)
		{
			Destroy(CarriedObj.gameObject);
			CarriedObj = null;
			if (ItemCarried == ItemType.Key)
				Instantiate(keyPrefab, KeyCard.position, Quaternion.identity);

			FindObjectOfType<Map>().exitDoor.unlocked = false;
		}
		ItemCarried = ItemType.None;

		if (gf != null)
		{
			float f = 0;
			float from = 3.5f;
			float to = -2.29f;

			while (f < 1)
			{
				f += Time.deltaTime * 1f;

				gf.height = Mathf.SmoothStep(from, to, f);

				yield return null;
			}
			gf.height = to;
		}
	}
}
