﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BaseBlock : MonoBehaviour
{
    public int x, y;
    public BlockType type;
}
