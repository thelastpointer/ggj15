﻿using System;
using UnityEngine;

public class Door : BaseBlock
{
    static string DoorPart1 = "door-01";
    static string DoorPart2 = "door-02";

    Transform doorPart1;
    Transform doorPart2;

    float startX1;
    float startX2;

    float distance = 1f;
    public float t = 0.0f;

    public bool IsOpen = false;
    public bool IsHorizontal = true;

    float checkTimer;

    public Light light;
    bool lastUnlocked;
    public bool unlocked;

    void Start()
    {
        doorPart1 = transform.FindChild(DoorPart1);
        doorPart2 = transform.FindChild(DoorPart2);

        startX1 = doorPart1.transform.localPosition.x;
        startX2 = doorPart2.transform.localPosition.x;

        unlocked = false;
        lastUnlocked = false;
    }

    void Update()
    {
        float target1X = startX1 + distance;
        float target2X = startX2 + -distance;

        if (!IsOpen)
            t -= Time.deltaTime;
        else
            t += Time.deltaTime;

        t = t < 0 ? 0 : t;
        t = t > 1f ? 1f : t;

        float x1 = Mathf.SmoothStep(startX1, target1X, t);
        float x2 = Mathf.SmoothStep(startX2, target2X, t);

        Vector3 newPos1 = doorPart1.transform.localPosition;
        newPos1.x = x1;
        doorPart1.transform.localPosition = newPos1;

        Vector3 newPos2 = doorPart2.transform.localPosition;
        newPos2.x = x2;
        doorPart2.transform.localPosition = newPos2;

        if (checkTimer < 0.0f && unlocked)
        {
            GameObject[] imps = GameObject.FindGameObjectsWithTag("Imp");
            bool open = false;
            for (int i = 0; i < imps.Length; ++i)
            {
                if (Vector3.Distance(imps[i].transform.position, transform.position) < 2)
                {
                    open = true;
                    break;
                }
            }
            IsOpen = open;
            checkTimer = 0.1f;
        }
        checkTimer -= Time.deltaTime;

        if (unlocked != lastUnlocked)
        {
            updateLight();
        }
    }

    public void Close()
    {
        IsOpen = false;
    }

    public void Open()
    {
        IsOpen = true;
    }

    public void Toggle()
    {
        IsOpen = !IsOpen;
    }

    void updateLight()
    {
        if (light != null)
        {
            light.color = unlocked ? new Color(0, 1, 0) : new Color(1,0,0);
        }
        lastUnlocked = unlocked;
    }
}