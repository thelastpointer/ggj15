﻿using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

[System.Flags]
public enum BlockType
{
    None            = 0x0,

    NonWalkable		= 0x0001,
    Walkable		= 0x0002,

	DEATH			= 0x00f0,
	Spike			= 0x0010,
	Lava			= 0x0020,
	Fall			= 0x0040,
}

public enum PowerupType
{
    None = 0,
    Key = 1,
    Weapon = 2,
    TurretUp = 3,
    TurretLeft = 4
}

public class Map : MonoBehaviour
{
    public List<Texture2D> mapTextures;
    public List<MapInfo> mapInfo = new List<MapInfo>
    {
		new MapInfo(3, new List<Tuple<PowerupType, Vector2i>>() 
        {
			new Tuple<PowerupType,Vector2i>(PowerupType.Key, new Vector2i(10,10))
		}),
		new MapInfo(5, new List<Tuple<PowerupType, Vector2i>>() 
        {
			new Tuple<PowerupType, Vector2i>(PowerupType.Key, new Vector2i(6,24))
		}),
		new MapInfo(3, new List<Tuple<PowerupType, Vector2i>>() 
        {
			new Tuple<PowerupType, Vector2i>(PowerupType.Key, new Vector2i(1,15))
		}),
		new MapInfo(5, new List<Tuple<PowerupType,Vector2i>>() 
        {
			new Tuple<PowerupType, Vector2i>(PowerupType.Key, new Vector2i(29,16)),
			new Tuple<PowerupType, Vector2i>(PowerupType.Weapon, new Vector2i(25,18)),
			new Tuple<PowerupType, Vector2i>(PowerupType.TurretUp, new Vector2i(4,25)),
			new Tuple<PowerupType, Vector2i>(PowerupType.TurretLeft, new Vector2i(25,23))
		})
    }; 
    
    public int currentMapIndex;

    public Transform mapPrefabs;

    public int width;
    public int height;
    [SerializeField] [HideInInspector] BlockType[] mapGrid;

    public Door startDoor;
    public Door exitDoor;

    public int addedImps;

    public Dictionary<Color, Tuple<string, int, string>> blockTypes = new Dictionary<Color, Tuple<string, int, string>>()
    {
        {new Color32(255,255,255,255), new Tuple<string, int, string>("floor-01", 0, "")}, // #FFFFFF
        {new Color32(222,222,222,255), new Tuple<string, int, string>("floor-02", 0, "")}, // #DEDEDE
        {new Color32(0,157,194,255), new Tuple<string, int, string>("lift-01", 0, "lift-from")}, // #009DC2
        {new Color32(0,95,121,255), new Tuple<string, int, string>("lift-01", 0, "lift-to")}, // #005F79
            
        {new Color32(190,162,255,255), new Tuple<string, int, string>("path-01", 0, "")}, // #BEA2FF
            
        {new Color32(48,48,48,255), new Tuple<string, int, string>("trap-01", 0, "")}, // #303030
        {new Color32(255,0,0,255), new Tuple<string, int, string>("lava-01", 0, "")}, // #FF0000
            
        {new Color32(0,64,0,255), new Tuple<string, int, string>("warning-01", 0, "")}, // #004000
        {new Color32(0,64,30,255), new Tuple<string, int, string>("warning-01", -90, "")}, // #00401E
        {new Color32(0,64,60,255), new Tuple<string, int, string>("warning-01", -180, "")}, // #00403C
        {new Color32(0,64,90,255), new Tuple<string, int, string>("warning-01", -270, "")}, // #00405A
            
        {new Color32(192,192,192,255), new Tuple<string, int, string>("button-01", 0, "button")}, // #C0C0C0
            
        {new Color32(0,32,0,255), new Tuple<string, int, string>("warning-02", 0, "")}, // #002000 |''
        {new Color32(0,32,30,255), new Tuple<string, int, string>("warning-02", -90, "")}, // #00201E |_
        {new Color32(0,32,60,255), new Tuple<string, int, string>("warning-02", -180, "")}, // #00203C _|
        {new Color32(0,32,90,255), new Tuple<string, int, string>("warning-02", -270, "")}, // #00205A ''|
                
        {new Color32(0,129,0,255), new Tuple<string, int, string>("edge-01", 0, "")}, // #008100 |''
        {new Color32(0,129,30,255), new Tuple<string, int, string>("edge-01", -90, "")}, // #00811E |_
        {new Color32(0,129,60,255), new Tuple<string, int, string>("edge-01", -180, "")}, // #00813C _|
        {new Color32(0,129,90,255), new Tuple<string, int, string>("edge-01", -270, "")}, // #00815A ''|
            
        {new Color32(32,255,0,255), new Tuple<string, int, string>("innerwall-01", 0, "")}, // #20FF00
        {new Color32(32,255,30,255), new Tuple<string, int, string>("innerwall-01", -90, "")}, // #20FF1E
        {new Color32(32,255,60,255), new Tuple<string, int, string>("innerwall-01", -180, "")}, // #20FF3C
        {new Color32(32,255,90,255), new Tuple<string, int, string>("innerwall-01", -270, "")}, // #20FF5A
            
        {new Color32(128,128,0,255), new Tuple<string, int, string>("wall-01", 0, "")}, // #808000
        {new Color32(128,128,90,255), new Tuple<string, int, string>("wall-01", -270, "")}, // #80805A
            
        {new Color32(128,96,0,255), new Tuple<string, int, string>("wall-02", 0, "")}, // #806000
        {new Color32(128,96,90,255), new Tuple<string, int, string>("wall-02", -270, "")}, // #80605A
            
        {new Color32(128,64,0,255), new Tuple<string, int, string>("wall-03", 0, "")}, // #804000
        {new Color32(128,64,90,255), new Tuple<string, int, string>("wall-03", -270, "")}, // #80405A
        
        {new Color32(128,32,0,255), new Tuple<string, int, string>("wall-04", 0, "")}, // #802000
        {new Color32(128,32,90,255), new Tuple<string, int, string>("wall-04", -270, "")}, // #80205A
                
        {new Color32(64,64,0,255), new Tuple<string, int, string>("walledge-01", 0, "")}, // #404000 |''
        {new Color32(64,64,30,255), new Tuple<string, int, string>("walledge-01", -90, "")}, // #40401E |_
        {new Color32(64,64,60,255), new Tuple<string, int, string>("walledge-01", -180, "")}, // #40403C _|
        {new Color32(64,64,90,255), new Tuple<string, int, string>("walledge-01", -270, "")}, // #40405A ''|
            
        {new Color32(64,32,0,255), new Tuple<string, int, string>("inneredge-01", 0, "")}, // #402000 |''
        {new Color32(64,32,30,255), new Tuple<string, int, string>("inneredge-01", -90, "")}, // #40201E |_
        {new Color32(64,32,60,255), new Tuple<string, int, string>("inneredge-01", -180, "")}, // #40203C _|
        {new Color32(64,32,90,255), new Tuple<string, int, string>("inneredge-01", -270, "")}, // #40205A ''|
        
        {new Color32(0,255,0,255), new Tuple<string, int, string>("entrance-01", 0, "entrance")}, // #00FF00
        {new Color32(0,255,90,255), new Tuple<string, int, string>("entrance-01", -270, "entrance")}, // #00FF5A
        
        {new Color32(255,140,0,255), new Tuple<string, int, string>("entrance-01", 0, "exit")}, // #FF8C00
        {new Color32(255,140,90,255), new Tuple<string, int, string>("entrance-01", -270, "exit")}, // #FF8C5A
            
        {new Color32(0,128,0,255), new Tuple<string, int, string>("door-01", 0, "door")}, // #008000
        {new Color32(0,128,30,255), new Tuple<string, int, string>("door-01", -90, "door")}, // #00801E |--
        {new Color32(0,128,60,255), new Tuple<string, int, string>("door-01", -180, "door")}, // #00803C
        {new Color32(0,128,90,255), new Tuple<string, int, string>("door-01", -270, "door")}, // #00805A --|
    };

    public Dictionary<string, Transform> blockMap = new Dictionary<string, Transform>(); 

    void Start()
    {
    }

    void Update()
    {
    }

    public void Load()
    {
        Texture2D texture = mapTextures[currentMapIndex];
        width = texture.width;
        height = texture.height;

        for (int i = 0; i < transform.childCount; ++i)
        {
            Transform child = transform.GetChild(i);
            DestroyImmediate(child.gameObject);
            i--;
        }

        blockMap.Clear();
        foreach (Transform child in mapPrefabs)
        {
            blockMap.Add(child.name, child);
        }

        mapGrid = new BlockType[width * height];
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                 CreateBlock(i, j, texture.GetPixel(i, j));
            }    
        }

        startDoor.IsHorizontal = GetBlockType(startDoor.x - 1, startDoor.y) == BlockType.None;
        exitDoor.IsHorizontal = GetBlockType(exitDoor.x - 1, exitDoor.y) == BlockType.None;
    }

    public void LoadScene()
    {
        Application.LoadLevel(currentMapIndex);
    }

    public void LoadNext()
    {
        currentMapIndex++;

        LoadScene();
    }

    public void LoadPrev()
    {
        if (currentMapIndex > 0)
            currentMapIndex--;

        Load();
    }

    public BlockType GetBlockType(int x, int y)
    {
		int i = x + width * y;
        if (x >= width || x < 0 || y < 0 || y > height)
        {
            Debug.Log("Shouldn't happen?");
            return BlockType.NonWalkable;
        }
        return mapGrid[i];
    }

    public void SetBlockType(int x, int y, BlockType type)
    {
		int i = x + width * y;
        if (x >= width || x < 0 || y < 0 || y > height)
        {
            Debug.Log("Shouldn't happen?");
            return;
        }
        mapGrid[i] = type;
    }

	public Vector2 WorldToMap(Vector3 pos)
	{
		return new Vector2(Mathf.RoundToInt(-pos.x), Mathf.RoundToInt(-pos.z));
	}
	public Vector3 MapToWorld(int x, int y)
	{
		return new Vector3(-x, 0, -y);
	}

    public MapInfo GetCurrentMapInfo()
    {
        return mapInfo[currentMapIndex];
    }

    void ProcessBlockType(int x, int y, string name)
    {
        BlockType blockType = BlockType.Walkable;

        switch (name) 
        {
            case "wall-01":
            case "wall-02":
            case "innerwall-01":
            case "inneredge-01":
            case "inneredge-02":
            case "walledge-01":
            case "walledge-02":
                blockType = BlockType.NonWalkable;
                
            break;
        }

        switch (name)
        {
            case "trap-01":
                blockType |= BlockType.Spike;
                break;
            case "lava-01":
                blockType |= BlockType.Lava;
                break;

            //empty block
            case "":
                blockType |= BlockType.Fall;
                break;
        }

        SetBlockType(x, y, blockType);
    }

    void AddScripts(int x, int y, Transform block, Tuple<string, int, string> blockInfo)
    {
        if (block.name.Contains("entrance-01"))
        {
            Door door = block.gameObject.AddComponent<Door>();
            if (blockInfo.Item3 == "entrance")
            {
                startDoor = door;
            }
            if (blockInfo.Item3 == "exit")
            {
                exitDoor = door;
            }
            door.x = x;
            door.y = y;
            door.type = GetBlockType(x, y);
        }
    }

    void CreateBlock(int x, int y, Color32 color)
    {
        Transform prefab = GetPrefabByColor(color);
        if (prefab == null)
            return;

        Transform block = Instantiate(prefab) as Transform;

        Tuple<string, int, string> blockInfo;
        if (blockTypes.TryGetValue(color, out blockInfo))
        {
            ProcessBlockType(x, y, blockInfo.Item1);
            block.transform.Rotate(0, 0, blockInfo.Item2);
        }
        else
        {
            ProcessBlockType(x, y, "");
        }

        AddScripts(x, y, block, blockInfo);
        block.transform.position = new Vector3(-x, 0, -y);
        block.transform.parent = transform;
    }

    Transform GetPrefabByColor(Color32 color)
    {
        Tuple<string, int, string> prefab;
        if (blockTypes.TryGetValue(color, out prefab))
        {
            Transform blockPrefab;
            blockMap.TryGetValue(prefab.Item1, out blockPrefab);
            return blockPrefab;
        }

        return null;
    } 
}