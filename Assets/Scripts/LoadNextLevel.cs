﻿using UnityEngine;
using System.Collections;

public class LoadNextLevel : MonoBehaviour
{
	public string LevelName = "";

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<PlayerControls>() != null)
		{
			/*if (string.IsNullOrEmpty(LevelName))
				Application.LoadLevel(Application.loadedLevel);
			else
				Application.LoadLevel(LevelName);
			*/
			StartCoroutine(LoadLevel());
		}
	}

	IEnumerator LoadLevel()
	{
		Debug.Log("FOG::LoadNextLevel");
		GlobalFog gf = FindObjectOfType<GlobalFog>();
		if (gf != null)
		{
			float f = 0;
			float from = -2.29f;
			float to = 3.5f;
			while (f < 1)
			{
				f += Time.deltaTime * 1f;

				gf.height = Mathf.SmoothStep(from, to, f);

				yield return null;
			}
			gf.height = to;
		}

		if (string.IsNullOrEmpty(LevelName))
			Application.LoadLevel(Application.loadedLevel);
		else
			Application.LoadLevel(LevelName);
	}
}
