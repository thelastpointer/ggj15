﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
	static Music instance;

	void Start()
	{
		if (instance != null)
		{
			if (instance.GetComponent<AudioSource>().clip != this.GetComponent<AudioSource>().clip)
			{
				instance.GetComponent<AudioSource>().clip = this.GetComponent<AudioSource>().clip;
				instance.GetComponent<AudioSource>().Play();
			}
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}
}
