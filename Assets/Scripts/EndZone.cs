﻿using UnityEngine;

public class EndZone : MonoBehaviour
{
    LevelStateManager stateManager;

    public void OnEnable()
    {
        stateManager = FindObjectOfType<LevelStateManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != null && stateManager != null)
        {
			Application.LoadLevel(Application.loadedLevel + 1);
        }
    }
}