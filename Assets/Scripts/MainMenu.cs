﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	public string[] URLs;
	public GameObject Main, Credits;
	public AudioClip Click;

	public void NewGame()
	{
		Application.LoadLevel(1);
	}

	public void GotoMain()
	{
		Main.SetActive(true);
		Credits.SetActive(false);
	}

	public void GotoCredits()
	{
		Main.SetActive(false);
		Credits.SetActive(true);
	}

	public void OpenLink(int idx)
	{
		Application.OpenURL(URLs[idx]);
	}

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
			GetComponent<AudioSource>().PlayOneShot(Click);

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (Credits.activeSelf)
				GotoMain();
			else
				Application.Quit();
		}
	}
}
