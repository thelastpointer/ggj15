using System;
using UnityEngine;

[Serializable]
public struct Vector3i
{

    public static readonly Vector3i zero = new Vector3i(0, 0, 0);
    public static readonly Vector3i one = new Vector3i(1, 1, 1);
    public static readonly Vector3i forward = new Vector3i(0, 0, 1);
    public static readonly Vector3i back = new Vector3i(0, 0, -1);
    public static readonly Vector3i up = new Vector3i(0, 1, 0);
    public static readonly Vector3i down = new Vector3i(0, -1, 0);
    public static readonly Vector3i left = new Vector3i(-1, 0, 0);
    public static readonly Vector3i right = new Vector3i(1, 0, 0);
    public static readonly Vector3i infinite = new Vector3i(int.MaxValue, int.MaxValue, int.MaxValue);

    public int x;
    public int y;
    public int z;

    public Vector3i(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public Vector3i(Vector3i v)
    {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }

    public Vector3i(Vector3 v)
    {
        this.x = (int) v.x;
        this.y = (int) v.y;
        this.z = (int) v.z;
    }

    public static Vector3i Scale(Vector3i a, Vector3i b)
    {
        return new Vector3i(a.x * b.x, a.y * b.y, a.z * b.z);
    }

    public static float Distance(Vector3i a, Vector3i b)
    {
        return Mathf.Sqrt(DistanceSquared(a, b));
    }

    public static int DistanceSquared(Vector3i a, Vector3i b)
    {
        int dx = b.x - a.x;
        int dy = b.y - a.y;
        int dz = b.z - a.z;
        return dx * dx + dy * dy + dz * dz;
    }

    public override string ToString()
    {
        return string.Format("Vector3i({0} {1} {2})", x, y, z);
    }

    public static bool operator ==(Vector3i a, Vector3i b)
    {
        return a.x == b.x && a.y == b.y && a.z == b.z;
    }

    public static bool operator !=(Vector3i a, Vector3i b)
    {
        return a.x != b.x || a.y != b.y || a.z != b.z;
    }

    public static bool operator <(Vector3i a, Vector3i b)
    {
        return a.x < b.x && a.y < b.y && a.z < b.z;
    }

    public static bool operator >(Vector3i a, Vector3i b)
    {
        return a.x > b.x && a.y > b.y && a.z > b.z;
    }

    public static bool operator <=(Vector3i a, Vector3i b)
    {
        return a.x <= b.x && a.y <= b.y && a.z <= b.z;
    }

    public static bool operator >=(Vector3i a, Vector3i b)
    {
        return a.x >= b.x && a.y >= b.y && a.z >= b.z;
    }

    public static Vector3i operator -(Vector3i a, Vector3i b)
    {
        return new Vector3i(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static Vector3i operator +(Vector3i a, Vector3i b)
    {
        return new Vector3i(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3i operator *(Vector3i a, int d)
    {
        return new Vector3i(a.x * d, a.y * d, a.z * d);
    }

    public static Vector3i operator *(int d, Vector3i a)
    {
        return new Vector3i(a.x * d, a.y * d, a.z * d);
    }

    public static explicit operator Vector3(Vector3i v)
    {
        return new Vector3(v.x, v.y, v.z);
    }

    public static explicit operator Vector3i(Vector3 v)
    {
        return new Vector3i(v);
    }

    public static Vector3i Abs(Vector3i v)
    {
        return new Vector3i(Mathf.Abs(v.x), Math.Abs(v.y), Math.Abs(v.z));
    }

    public static Vector3i Min(Vector3i lhs, Vector3i rhs)
    {
        return new Vector3i(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z));
    }

    public static Vector3i Max(Vector3i lhs, Vector3i rhs)
    {
        return new Vector3i(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z));
    }

    public static Vector3i Clamp(Vector3i value, Vector3i min, Vector3i max)
    {
        return new Vector3i(Mathf.Clamp(value.x, min.x, max.x), Mathf.Clamp(value.y, min.y, max.y), Mathf.Clamp(value.z, min.z, max.z));
    }

    public override int GetHashCode()
    {
        return x ^ (y << 8) ^ (z << 16);
    }

    public override bool Equals(object other)
    {
        if ( !(other is Vector3i) )
            return false;

        Vector3i vector = (Vector3i) other;
        return x == vector.x && y == vector.y && z == vector.z;
    }
}