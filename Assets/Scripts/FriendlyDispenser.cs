﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FriendlyDispenser : MonoBehaviour
{
    public float MaxDispenseTime = 10f;

    public float dispenseTime;
    public float radius = 2f;

    public Transform packagePrefab;

    bool busy = false;
    GameObject pack;

    public Transform Lift;
    public AudioClip Release, Release2;
    Transform package;

    public bool hasCube;

    public void Start()
    {
        dispenseTime = MaxDispenseTime;
        hasCube = false;
    }

    public void Update()
    {
        if (!hasCube && dispenseTime <= 0.0f)
        {
            SpawnFriendlyPackage();
            dispenseTime = MaxDispenseTime;
        }

        dispenseTime -= Time.deltaTime;
    }

    void SpawnFriendlyPackage()
    {
        StartCoroutine(LiftAnim());
        hasCube = true;
    }

    IEnumerator LiftAnim()
    {
        busy = true;

        float startTime = Time.time;

        Vector3 pos = Lift.position;
        OneShotSound.Play(Release, Vector3.zero);
        while ((Time.time - startTime) < 1)
        {
            Lift.transform.position = Vector3.Lerp(pos, pos + new Vector3(0, -1, 0), (Time.time - startTime));
            yield return null;
        }
        while ((Time.time - startTime) < 2)
        {
            yield return null;
        }

        package = Instantiate(packagePrefab, Lift.transform.position + new Vector3(0, -0.5f, 0), Quaternion.identity) as Transform;
        package.GetComponent<Package>().dispenser = this;

        Vector3 pos2 = package.transform.position;
        pos2.y = 0.2f;

        bool playedSound = false;
        while ((Time.time - startTime) < 3)
        {
            Lift.transform.position = Vector3.Lerp(pos + new Vector3(0, -2, 0), pos, (Time.time - startTime - 2));

            package.transform.position = Vector3.Lerp(pos2 + new Vector3(0, -2, 0), pos2, (Time.time - startTime - 2));

            if (!playedSound && ((Time.time - startTime) > 2.5f))
            {
                OneShotSound.Play(Release, Vector3.zero);
                playedSound = true;
            }

            yield return null;
        }

        busy = false;
    }

}
