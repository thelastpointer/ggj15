﻿/*
	ControlButton.cs
	Sends a message when clicked (used for direction controls)
*/

using UnityEngine;
using System.Collections;

public class ControlButton : MonoBehaviour
{
	public GameObject Target;
	public Direction Dir;

	Transform tr;
	Renderer rend;
	Vector3 originalScale;

	void Awake()
	{
		tr = GetComponent<Transform>();
		rend = GetComponent<Renderer>();
		originalScale = tr.localScale;
		tr.localScale = originalScale * 0.8f;
		rend.material.color = new Color(1f, 1f, 1f, 0.8f);
	}

	void OnMouseUpAsButton()
	{
		OnMouseExit();
		Target.SendMessage("ControlClicked", Dir);
	}

	void OnMouseEnter()
	{
		tr.localScale = originalScale;
		rend.material.color = Color.white;
	}
	void OnMouseExit()
	{
		tr.localScale = originalScale * 0.8f;
		rend.material.color = new Color(1f, 1f, 1f, 0.8f);
	}
	void OnEnable()
	{
		OnMouseExit();
	}

	public enum Direction
	{
		None,
		Up,
		Down,
		Left,
		Right
	}
}
