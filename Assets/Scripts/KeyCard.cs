﻿using UnityEngine;

public class KeyCard : PickupAble
{
    Map map;
    public static Vector3 position;

    public void Start()
    {
        map = FindObjectOfType<Map>();
        position = transform.position;
    }


//    public void OnEnabled()
//    {
//        player = FindObjectOfType<PlayerControls>();
//    }



    public override void OnPickUp()
    {
        map.exitDoor.unlocked = true;
        player.ItemCarried = ItemType.Key;
    }

    public override void OnDropped()
    {
        map.exitDoor.unlocked = false;
        player.ItemCarried = ItemType.None;
    }
}
