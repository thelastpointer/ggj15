using System;
using UnityEngine;

[Serializable]
public struct Vector2i
{

    public static readonly Vector2i zero = new Vector2i(0, 0);
    public static readonly Vector2i one = new Vector2i(1, 1);

    public int x;
    public int y;

    public Vector2i(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2i(Vector2i v)
    {
        this.x = v.x;
        this.y = v.y;
    }

    public Vector2i(Vector2 v)
    {
        this.x = (int) v.x;
        this.y = (int) v.y;
    }

    public override string ToString()
    {
        return string.Format("Vector3i({0} {1})", x, y);
    }

    public static bool operator ==(Vector2i a, Vector2i b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Vector2i a, Vector2i b)
    {
        return a.x != b.x || a.y != b.y;
    }

    public static bool operator <(Vector2i a, Vector2i b)
    {
        return a.x < b.x && a.y < b.y;
    }

    public static bool operator >(Vector2i a, Vector2i b)
    {
        return a.x > b.x && a.y > b.y;
    }

    public static bool operator <=(Vector2i a, Vector2i b)
    {
        return a.x <= b.x && a.y <= b.y;
    }

    public static bool operator >=(Vector2i a, Vector2i b)
    {
        return a.x >= b.x && a.y >= b.y;
    }

    public static Vector2i operator -(Vector2i a, Vector2i b)
    {
        return new Vector2i(a.x - b.x, a.y - b.y);
    }

    public static Vector2i operator +(Vector2i a, Vector2i b)
    {
        return new Vector2i(a.x + b.x, a.y + b.y);
    }

    public static Vector2i operator *(Vector2i a, int d)
    {
        return new Vector2i(a.x * d, a.y * d);
    }

    public static Vector2i operator *(int d, Vector2i a)
    {
        return new Vector2i(a.x * d, a.y * d);
    }

    public static explicit operator Vector3(Vector2i v)
    {
        return new Vector3(v.x, v.y);
    }

    public static explicit operator Vector2i(Vector3 v)
    {
        return new Vector2i(v);
    }

    public override int GetHashCode()
    {
        return x ^ (y << 8);
    }

    public override bool Equals(object other)
    {
        if ( !(other is Vector3i) )
            return false;

        Vector3i vector = (Vector3i) other;
        return x == vector.x && y == vector.y;
    }
}