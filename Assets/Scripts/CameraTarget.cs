﻿using System;
using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public SecurityCamera camera;

    public Transform Spot;

    public float targetRadius = 2.0f;
    public float targetAlpha = 0.3f;
    public float radius;
    float alpha;


    void Start()
    {
    }

    void OnEnable()
    {
    }

    void Update()
    {
        radius = Mathf.Lerp(radius, targetRadius, Time.deltaTime);
        transform.localScale = new Vector3(radius, 1, radius);

        alpha = Mathf.Lerp(alpha, targetAlpha, Time.deltaTime);
        SetAlpha(alpha);

        if (camera.IsPlayerDivergent && Math.Abs(radius - 0.5f) < 0.2f)
        {
            camera.player.Death();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        camera.IsPlayerVisible = true;
    }

    public void OnTriggerExit(Collider other)
    {
        camera.IsPlayerVisible = false;
        targetRadius = 2.0f;
        targetAlpha = 0.3f;
    }


    void SetAlpha(float alpha)
    {
        Color color = Spot.gameObject.GetComponent<Renderer>().material.color;
        color.a = alpha;
        Spot.gameObject.GetComponent<Renderer>().material.color = color;
    }

}