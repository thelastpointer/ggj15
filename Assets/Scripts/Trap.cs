﻿using UnityEngine;

public class Trap : MonoBehaviour
{
    public GameObject corpsePrefab;

    PlayerControls player;

    public void OnEnable()
    {
        player = FindObjectOfType<PlayerControls>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject != null)
        {
            Transform transform = other.transform;
            if (corpsePrefab != null) 
                Instantiate(corpsePrefab, transform.position, transform.rotation);
            player.Death();
        }
    }
}