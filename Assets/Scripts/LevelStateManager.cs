﻿using System;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class LevelStateManager : MonoBehaviour
{
    [SerializeField] Map map;

    public MapState state;
    public int startingImps;
    public int addedImps;
    public int survivedImps;
    public int diedImps;

    public Transform impPrefab;

    public const float SpawnTime = 3f;
    public float spawnTimer;

    List<Transform> impWrecks;

    public Text left;
    public Text right;

    float time;

    public enum MapState
    {
        Intro,
        Running,
        Outro
    }

    void Awake()
    {
        Init();
    }

    void Update()
    {
//        if (Input.GetKeyDown(KeyCode.A))
//        {
//             map.startDoor.Toggle();
//        }
//        if (Input.GetKeyDown(KeyCode.B))
//        {
//            map.exitDoor.Toggle();
//        }

        if (state == MapState.Intro)
        {
//            map.startDoor.Open();
//            if (addedImps < startingImps)
//            {
//                if (spawnTimer < 0.0f)
//                {
//                    SpawnImp();
//                    spawnTimer = SpawnTime;
//                }
//
//                spawnTimer -= Time.deltaTime;
//            }
//            else
//            {
//                state = MapState.Running;
//                map.startDoor.Close();
//            }
        }

        if (state == MapState.Running && diedImps + survivedImps >= startingImps)
        {
            state = MapState.Outro;
            LoadNext();
        }

        time += Time.deltaTime;
        int ms = (int) ((time - ((int) time)) * 100);
        int s = (int) time;
        int m = (int) (time / 60);
        right.text = String.Format("{0:00} {1:00} {2:00}", m, s, ms);
    }

    public void LoadLevel(int i)
    {
        Clear();
        map = GetComponent<Map>();
        map.currentMapIndex = i;
        map.Load();
        Init();
    }

    public void LoadNext()
    {
        Clear();
        map = GetComponent<Map>();
        map.LoadNext();
        Init();
    }

    public void LoadPrev()
    {
        Clear();
        map = GetComponent<Map>();
        map.LoadPrev();
        Init();
    }

    public void Clear()
    {
        for (int i = 0; i < impWrecks.Count; ++i)
        {
            Destroy(impWrecks[i]);
        }
        impWrecks.Clear();
        Imp_basic[] imps = FindObjectsOfType<Imp_basic>();
        for (int i = 0; i < imps.Length; ++i)
        {
            Destroy(imps[i]);
        }
    }

    void Init()
    {
        state = MapState.Running;
        MapInfo mapInfo = map.GetCurrentMapInfo();
        startingImps = mapInfo.startingImps;
        addedImps = 0;
        diedImps = 0;
        impWrecks = new List<Transform>();
    }

    public void SpawnImp()
    {
        addedImps++;

        Transform impTransform = SpawnObject(map.startDoor.x, map.startDoor.y, impPrefab);
        Imp_basic imp = impTransform.GetComponent<Imp_basic>();
        imp.SetDirection(map.startDoor.IsHorizontal ? ControlButton.Direction.Down : ControlButton.Direction.Left);
    }

    public void ImpSurvived()
    {
        survivedImps++;
    }

    public void ImpDied(Transform wreck)
    {
        impWrecks.Add(wreck);
        diedImps++;
    }

    public Transform SpawnObject(int x, int y, Object obj)
    {
        return Instantiate(obj, map.MapToWorld(x, y), Quaternion.identity) as Transform;
    }
}