﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class MapInfo
    {
        public int startingImps;
        public List<Tuple<PowerupType, Vector2i>> powerUps;

        public MapInfo(int startingImps, List<Tuple<PowerupType, Vector2i>> powerUps)
        {
            this.startingImps = startingImps;
            this.powerUps = powerUps;
        }
    }
}
