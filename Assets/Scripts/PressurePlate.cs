﻿using UnityEngine;
using System.Collections;

public class PressurePlate : MonoBehaviour
{
	public Door[] Targets;
	public Door2[] Targets2;
	public Renderer GFX;
	public Material Material_On, Material_Off;
	public AudioClip PackageDropSound;

	int insideObjects = 0; // How many objects are inside -- so a leaving player wont mess up a dropped package
	void OnTriggerEnter(Collider other)
	{
		// Player or package activates; package stays
		if (other.GetComponent<Package>() != null)
		{
			other.transform.parent = transform;
			other.transform.localPosition = new Vector3(0, -0.145f, 0);
			other.GetComponent<Collider>().enabled = false;
			FindObjectOfType<PlayerControls>().IsCarrying = false;
			OneShotSound.Play(PackageDropSound, transform.position);
			HandleButton(1);
		}
		else if (other.GetComponent<PlayerControls>() != null)
		{
			HandleButton(1);
		}

	}
	void OnTriggerExit(Collider other)
	{
		if ((other.GetComponent<Package>() != null) || (other.GetComponent<PlayerControls>() != null))
		{
			HandleButton(-1);
		}
	}

	void HandleButton(int add)
	{
		int oldValue = insideObjects;
		insideObjects += add;

		// Everything removed
		if ((insideObjects == 0) && (oldValue > 0))
		{
			foreach (Door d in Targets)
				d.Close();
			foreach (Door2 d in Targets2)
				d.Close();
			GFX.material = Material_Off;
		}
		// At least one thing placed
		else if ((insideObjects > 0) && (oldValue == 0))
		{
			foreach (Door d in Targets)
				d.Open();
			foreach (Door2 d in Targets2)
				d.Open();
			GFX.material = Material_On;
		}
	}
}
