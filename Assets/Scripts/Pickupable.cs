﻿using UnityEngine;
using System.Collections;

public class PickupAble : MonoBehaviour
{
    public AudioClip Grab;

    bool carried = false;

    public PlayerControls player;

    public void Start()
    {
        player = FindObjectOfType<PlayerControls>();
    }

    public void OnEnable()
    {
        player = FindObjectOfType<PlayerControls>();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (player.IsCarrying && carried)
            {
                carried = false;
                player.IsCarrying = false;
                transform.localPosition = Vector3.zero;
                transform.position = player.transform.position + player.lastLook.normalized * 0.9f;
                transform.parent = null;
                player.CarriedObj = null;
                player.ItemCarried = ItemType.None;
                OnDropped();
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (!carried && !player.IsCarrying)
        {
            if (other.GetComponent<CharacterController>() != null)
            {
                OneShotSound.Play(Grab, Vector3.zero);
                transform.parent = other.transform.Find("gfx");
                transform.localPosition = new Vector3(0, 0.4f, 0.5f);
                transform.localRotation = Quaternion.identity;
                //collider.enabled = false;
                carried = true;
                player.IsCarrying = true;
                player.CarriedObj = transform;
                OnPickUp();
            }
        }
    }

    public virtual void OnPickUp()
    {
        
    }

    public virtual void OnDropped()
    {

    }

    public void DestinationAnim()
    {
        //...
    }
}
