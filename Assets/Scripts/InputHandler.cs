﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour
{
	public GameObject Interface;
	public RectTransform ButtonUp, ButtonDown, ButtonLeft, ButtonRight;

	public void DirectionButton(int dir)
	{
		if (Imp_basic.Selected != null)
			Imp_basic.Selected.ControlClicked((ControlButton.Direction)dir);
	}

	Vector2 startMousePos;
	bool interfaceVisible = false;

	public void PopInterface(Vector3 pos)
	{
		RaycastHit hit;
		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50, 0xffff))
		{
			pos = hit.point;
			interfaceVisible = true;
			Interface.SetActive(true);
			Interface.transform.position = pos;

			startMousePos = Input.mousePosition;
		}
	}

	void Update()
	{
		if (interfaceVisible && !Input.GetMouseButton(0))
		{
			interfaceVisible = false;
			Interface.SetActive(false);

			if (Vector2.Distance(Input.mousePosition, startMousePos) > 10)
			{
				// Left side
				if (Input.mousePosition.x < startMousePos.x)
				{
					if (Input.mousePosition.y < startMousePos.y)
						Imp_basic.Selected.ControlClicked(ControlButton.Direction.Right);
					else
						Imp_basic.Selected.ControlClicked(ControlButton.Direction.Down);
				}
				// Right side
				else
				{
					if (Input.mousePosition.y < startMousePos.y)
						Imp_basic.Selected.ControlClicked(ControlButton.Direction.Up);
					else
						Imp_basic.Selected.ControlClicked(ControlButton.Direction.Left);
				}
			}
		}
		else if (interfaceVisible)
		{
			ButtonUp.localScale = Vector3.one;
			ButtonDown.localScale = Vector3.one;
			ButtonLeft.localScale = Vector3.one;
			ButtonRight.localScale = Vector3.one;

			// Left side
			if (Input.mousePosition.x < startMousePos.x)
			{
				if (Input.mousePosition.y < startMousePos.y)
					ButtonRight.localScale = Vector3.one * 1.2f;
				else
					ButtonDown.localScale = Vector3.one * 1.2f;
			}
			// Right side
			else
			{
				if (Input.mousePosition.y < startMousePos.y)
					ButtonUp.localScale = Vector3.one * 1.2f;
				else
					ButtonLeft.localScale = Vector3.one * 1.2f;
			}
		}
	}
}
