﻿/*
	CameraHandler.cs
*/

using UnityEngine;
using System.Collections;

public class CameraHandler : MonoBehaviour
{
	public bool UseImageEffects = false;
	public float Speed = 5;
	public int MouseBorder = 10;

	static CameraHandler instance;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		CameraHandler.instance.StartCoroutine(AnimFog(3.5f, -2.29f));

		Camera.main.GetComponent<Vignetting>().enabled = UseImageEffects;
		Camera.main.GetComponent<AntialiasingAsPostEffect>().enabled = UseImageEffects;
		Camera.main.GetComponent<ColorCorrectionCurves>().enabled = UseImageEffects;
	}

	void Update()
	{
		Vector3 dir = Vector3.zero;

		// Mouse movement
		if (Input.mousePosition.x < MouseBorder)
			dir = new Vector3(-1, dir.y, dir.z);
		if (Input.mousePosition.x > (Screen.width - MouseBorder))
			dir = new Vector3(1, dir.y, dir.z);
		if (Input.mousePosition.y < MouseBorder)
			dir = new Vector3(dir.x, dir.y, -1);
		if (Input.mousePosition.y > (Screen.height - MouseBorder))
			dir = new Vector3(dir.x, dir.y, 1);

		// Keyboard movement
		dir += new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

		// Actually move the cam
		dir = Quaternion.Euler(0, -135, 0) * dir;
		transform.Translate(dir * Speed * Time.deltaTime, Space.World);
	}
	
	public static void StartAnimation()
	{
		CameraHandler.instance.StartCoroutine(CameraHandler.instance.AnimFog(3.5f, -2.29f));
	}
	public static void EndAnimation()
	{
		CameraHandler.instance.StartCoroutine(CameraHandler.instance.AnimFog(-2.29f, 3.5f));
	}
	IEnumerator AnimFog(float from, float to)
	{
		Debug.Log("FOG::CameraHandler");
		GlobalFog gf = FindObjectOfType<GlobalFog>();
		if (gf != null)
		{
			float f = 0;
			while (f < 1)
			{
				f += Time.deltaTime * 1f;

				gf.height = Mathf.SmoothStep(from, to, f);

				yield return null;
			}
			gf.height = to;
		}
	}
}
