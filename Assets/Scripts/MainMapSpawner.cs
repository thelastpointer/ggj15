﻿using UnityEngine;

public class MainMapSpawner : MonoBehaviour
{
    public Transform prefabImp;
    public Transform prefabImpFlying;
    public float MaxSpawnTime = 10f;
	public float XRange = 10;
	public float ZRange = 10;
	public float AutoDestroyTime = 0;

    float spawnTime;

    void Start()
    {
        spawnTime = MaxSpawnTime;
    }

    void Update()
    {
        if (spawnTime < 0.0f)
        {
            Spawn();
            spawnTime = MaxSpawnTime;
        }

        spawnTime -= Time.deltaTime;
    }

    void Spawn()
    {
        Transform[] prefabs = {prefabImp, prefabImpFlying};
        Vector3 pos = transform.position + new Vector3(Random.value * XRange - (XRange / 2f), 0, Random.value * ZRange);
        Transform obj = Instantiate(prefabs[Random.Range(0, prefabs.Length - 1)], pos, Quaternion.Euler(-90, 0, 0)) as Transform;
        obj.gameObject.AddComponent<MenuImpPusher>();
		if (AutoDestroyTime > 0)
			Destroy(obj.gameObject, AutoDestroyTime);
    }
}