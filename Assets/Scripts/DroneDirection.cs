﻿using UnityEngine;
using System.Collections;

public class DroneDirection : MonoBehaviour
{
	public DirectionChange[] Changers;

	void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.name + " entered");

		Imp_basic imp = other.transform.root.GetComponent<Imp_basic>();
		if (imp != null)
		{
			foreach (DirectionChange dc in Changers)
			{
				Debug.Log(string.Format("{0} / {1}", imp.transform.rotation.eulerAngles.y, imp.Rotations[(int)dc.From]));
				if ((dc.From == imp.Direction) || Mathf.Approximately(imp.transform.rotation.eulerAngles.y, imp.Rotations[(int)dc.From]))
					imp.SetDirection(dc.To);
			}
		}
	}

	[System.Serializable]
	public class DirectionChange
	{
		public ControlButton.Direction From, To;
	}
}
