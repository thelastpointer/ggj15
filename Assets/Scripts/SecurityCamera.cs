﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using System.Collections;

public class SecurityCamera : MonoBehaviour
{
    public List<Vector3> points;
    public CameraTarget target;

    float time;
    public float f;

    public bool IsPlayerVisible;
    public bool IsPlayerDivergent;

    public PlayerControls player;

	void Start () 
    {
	
	}

    public void OnEnable()
    {
        player = FindObjectOfType<PlayerControls>();
        IsPlayerDivergent = false;
    }

	
	void Update ()
	{
	    Vector3 start = points[0];
        Vector3 end = points[1];

	    time += Time.deltaTime / 2f;
	    f = ((Mathf.Sin(time) + 1.0f) / 2f);

	    Vector3 pos = Vector3.Lerp(start, end, f);

        if (IsPlayerVisible)
        {
            IsPlayerDivergent = CheckPlayerBehaviour();
            if (IsPlayerDivergent)
            {
                pos = Vector3.Lerp(target.transform.position, player.transform.position, Time.deltaTime * 4);
                target.targetRadius = 0.5f;
                target.targetAlpha = 1.0f;
            }
            else
            {
                target.targetRadius = 2.0f;
                target.targetAlpha = 0.3f;
            }
        }

	    target.transform.position = pos;

        UpdateCamera();
    }

    public bool CheckPlayerBehaviour()
    {
        if (player.ItemCarried == ItemType.Key)
            return true;

        return false;
    }

    public void UpdateCamera()
    {
        Vector3 relativePos = target.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos) * Quaternion.Euler(270f, 0f, 180f);
        transform.rotation = rotation;
    }
}
