﻿/*
	Imp_basic.cs
	Simplest stupid imp dude
*/

using UnityEngine;
using System.Collections;

public class Imp_basic : MonoBehaviour
{
	public ImpState State = ImpState.Thinking;
	public Vector3 StartPosition, TargetPosition;
	public float Speed;
	public GameObject SpeechBubble;
	public GameObject AlertBubble;
	public GameObject SelectionMarker;
	public GameObject Interface;
	public Transform GFX;
	public float ThinkTime = 1f;
	public GameObject Corpse;

	public AudioClip[] SelectionSounds;
	public AudioClip AlertSound, ThinkSound;

	public static Imp_basic Selected = null;

	Transform tr;
	float actionStart;
	ControlButton.Direction nextAction;
	BlockType nextBlock;
    Map map;
    LevelStateManager levelStateManager;

	public ControlButton.Direction Direction { get { return nextAction; } }

    public int[] Rotations =
    {
        0,
        0,
        180,
        270,
        90
    };

	public enum ImpState
	{
		Moving,
		Thinking,
		Wandering
	}

    void Awake()
    {
        tr = GetComponent<Transform>();
        map = FindObjectOfType<Map>();
        levelStateManager = FindObjectOfType<LevelStateManager>();
    }

	void Start()
	{
		StartPosition = tr.position;
		TargetPosition = tr.position;
		SelectionMarker.SetActive(false);
		Interface.SetActive(false);
		SpeechBubble.SetActive(false);
		AlertBubble.SetActive(false);

		ChangeState(State);
	}

	void Update()
	{
		switch (State)
		{
			case ImpState.Moving:
				//tr.position = Vector3.Lerp(StartPosition, TargetPosition, Mathf.SmoothStep(0f, 1f, (Time.time - actionStart) * Speed));
//				tr.position = Vector3.Lerp(StartPosition, TargetPosition, (Time.time - actionStart) * Speed);
		        tr.position += tr.forward * Time.deltaTime;

				// Tilt forward when moving
				float f = -1 * Mathf.Abs(Mathf.SmoothStep(-1f, 1f, (Time.time - actionStart) * Speed)) + 1;
				GFX.localRotation = Quaternion.Euler(f * 10f, 0, 0);

				if (((Time.time - actionStart) * Speed) > 1)
				{
					StartPosition = TargetPosition;

					if (nextAction == ControlButton.Direction.Up)
						tr.rotation = Quaternion.Euler(0, 0, 0);
					else if (nextAction == ControlButton.Direction.Down)
						tr.rotation = Quaternion.Euler(0, 180, 0);
					else if (nextAction == ControlButton.Direction.Left)
						tr.rotation = Quaternion.Euler(0, 270, 0);
					else if (nextAction == ControlButton.Direction.Right)
						tr.rotation = Quaternion.Euler(0, 90, 0);

					TargetPosition = TargetPosition + tr.forward;

//					tr.position = StartPosition;

					nextAction = ControlButton.Direction.None;
					actionStart = Time.time;

					HandleMapEvents();
				}
				break;
			case ImpState.Thinking:
				if ((Time.time - actionStart) > ThinkTime)
				{
					tr.Rotate(0, 180, 0, Space.World);
//					TargetPosition = StartPosition + tr.forward;
					ChangeState(ImpState.Moving);
				}
				break;
			case ImpState.Wandering:
				//...
				break;
			default:
				break;
		}

		Interface.transform.rotation = Quaternion.identity;
		SpeechBubble.transform.rotation = Quaternion.Euler(45, -135, 0);
		AlertBubble.transform.rotation = Quaternion.Euler(45, -135, 0);
	}
	/*
	// Select/deselect
	void OnMouseUpAsButton()
	{
		if (Selected == this)
		{
			Selected = null;
			SelectionMarker.SetActive(false);
			Interface.SetActive(false);
		}
		else
		{
			if (Selected != null)
			{
				Selected.SelectionMarker.SetActive(false);
				Selected.Interface.SetActive(false);
			}

			Selected = this;
			SelectionMarker.SetActive(true);
			Interface.SetActive(true);
			SpeechBubble.SetActive(false);
			AlertBubble.SetActive(false);

			if (SelectionSounds != null)
				audio.PlayOneShot(SelectionSounds[Random.Range(0, SelectionSounds.Length)]);
		}
	}
	*/
	void OnMouseDown()
	{
		Selected = this;
		FindObjectOfType<InputHandler>().PopInterface(tr.position);
	}

	Vector2 Conv(float x, float y)
	{
		return new Vector2(-x, -y);
	}

	void HandleMapEvents()
	{
		if (map == null)
		{
			Debug.LogError("NO MAP");
			return;
		}

		Vector2 sp2 = map.WorldToMap(transform.position);
		Vector2 tp2 = map.WorldToMap(transform.position + transform.forward);
		
		int x = (int)sp2.x;
		int y = (int)sp2.y;
		int x2 = (int)tp2.x;
		int y2 = (int)tp2.y;
		nextBlock = map.GetBlockType(x2, y2);
		
		// Are we dead? On a DEATH tile?
		if ((map.GetBlockType(x, y) & BlockType.DEATH) != 0)
		{
			Transform wreck = Instantiate(Corpse, tr.position, tr.rotation) as Transform;
            levelStateManager.ImpDied(wreck);
			Destroy(gameObject);
			return;
		}

        //end block
	    if (x == map.exitDoor.x && y == map.exitDoor.y)
	    {
            levelStateManager.ImpSurvived();
            Destroy(gameObject);
            return;
	    }

		// Next tile
		if ((nextBlock & BlockType.Walkable) == 0)
			ChangeState(ImpState.Thinking);
		else
		{
			if ((nextBlock & BlockType.DEATH) != 0)
			{
				if (AlertSound != null)
					GetComponent<AudioSource>().PlayOneShot(AlertSound);
				AlertBubble.SetActive(true);
			}

			// Fiery lava death
			if ((nextBlock & BlockType.Lava) != 0)
			{
				TargetPosition += new Vector3(0, -0.5f, 0);
			}
			// Embarrassing drowning death
			// Trap death
			// Spikes
			else if ((nextBlock & BlockType.Spike) != 0)
			{
				TargetPosition += new Vector3(0, -0.5f, 0);
			}
			// Stupid falling death
			else if ((nextBlock & BlockType.Fall) != 0)
			{
			}

			// Key
			// Pressure plate
			// Weapon
		}
	}
	
	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1, 0, 0, 0.5f);
		Map map = FindObjectOfType<Map>();
		for (int x = 0; x < map.width; ++x)
		{
			for (int y = 0; y < map.width; ++y)
			{
				if ((map.GetBlockType(x, y) & BlockType.DEATH) != 0)
				{
					Gizmos.color = new Color(1, 0, 0, 0.5f);
					Gizmos.DrawCube(map.MapToWorld(x, y), new Vector3(0.5f, 0.5f, 0.5f));
				}
				else if ((map.GetBlockType(x, y) & BlockType.Walkable) != 0)
				{
					Gizmos.color = new Color(0, 1, 0, 0.5f);
					Gizmos.DrawCube(map.MapToWorld(x, y), new Vector3(0.5f, 0.5f, 0.5f));
				}
			}
		}
	}
	
	public void ControlClicked(ControlButton.Direction dir)
	{
		//Debug.Log("Next action: " + dir);
		nextAction = dir;
		Selected = null;
		SelectionMarker.SetActive(false);
		Interface.SetActive(false);

		if (SelectionSounds != null)
			GetComponent<AudioSource>().PlayOneShot(SelectionSounds[Random.Range(0, SelectionSounds.Length)]);
	}

	void ChangeState(ImpState newState)
	{
		//Debug.Log("Changing state from " + State + " to " + newState);
		switch (State)
		{
			case ImpState.Moving:
				break;
			case ImpState.Thinking:
				SpeechBubble.SetActive(false);
				break;
			case ImpState.Wandering:
				break;
			default:
				break;
		}

		switch (newState)
		{
			case ImpState.Moving:
				break;
			case ImpState.Thinking:
				SpeechBubble.SetActive(true);
				if (ThinkSound != null)
					GetComponent<AudioSource>().PlayOneShot(ThinkSound);
				break;
			case ImpState.Wandering:
				break;
			default:
				break;
		}

		actionStart = Time.time;
		State = newState;
	}

    public void SetDirection(ControlButton.Direction direction)
    {
        nextAction = direction;
        tr.rotation = Quaternion.Euler(0, Rotations[(int) direction], 0);
    }
}
