/*
	OneShotSound.cs
	Plays a sound at a location.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OneShotSound
{
    public static void Play(AudioClip clip, Vector3 pos)
    {
        Play(clip, pos, 1.0f);
    }

	public static void Play(AudioClip clip, Vector3 pos, float volume)
	{
		Play(clip, pos, volume, null);
	}

    public static void Play(AudioClip clip, Vector3 pos, float volume, Transform parent)
    {
        GameObject go = new GameObject("Sound_" + clip.name + " (dynamic)");
        go.transform.position = pos;
		if (parent != null)
			go.transform.parent = parent;
        AudioSource audio = go.AddComponent<AudioSource>();
        audio.volume = volume;
        audio.PlayOneShot(clip);

        GameObject.Destroy(go, clip.length);
    }
}
