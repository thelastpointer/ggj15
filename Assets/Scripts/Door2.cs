﻿using UnityEngine;
using System.Collections;

public class Door2 : MonoBehaviour
{
	public DoorPart[] Parts;
	public float OpenTime = 1;

	void Awake()
	{
		foreach (DoorPart d in Parts)
		{
			d.OriginalPosition = d.Part.position;
			d.Speed = d.OpenPosition.magnitude / OpenTime;
		}
	}

	public void Open()
	{
		StopAllCoroutines();
		StartCoroutine(Animate(true));
	}
	public void Close()
	{
		StopAllCoroutines();
		StartCoroutine(Animate(false));
	}

	IEnumerator Animate(bool opening)
	{
		float startTime = Time.time;
		while ((Time.time - startTime) < OpenTime)
		{
			float f = (Time.time - startTime) / OpenTime;

			foreach (DoorPart d in Parts)
			{
				if (opening)
				{
					d.Part.position = d.Part.position + d.OpenPosition.normalized * d.Speed * Time.deltaTime;
					if ((d.Part.position - (d.OriginalPosition + d.OpenPosition)).sqrMagnitude < 0.1f)
						d.Part.position = d.OriginalPosition + d.OpenPosition;
				}
				else
				{
					d.Part.position = d.Part.position - d.OpenPosition.normalized * d.Speed * Time.deltaTime;
					if ((d.Part.position - d.OriginalPosition).sqrMagnitude < 0.1f)
						d.Part.position = d.OriginalPosition;
				}
			}

			yield return null;
		}
	}

	void OnDrawGizmosSelected()
	{
		if ((Parts != null) && (Parts.Length > 0))
		{
			foreach (DoorPart p in Parts)
			{
				if (p.Part != null)
				{
					Gizmos.color = Color.green;
					Gizmos.DrawLine(p.Part.position, p.Part.position + p.OpenPosition);

					Gizmos.color = new Color(0, 1, 0, 0.5f);
					Gizmos.DrawCube(p.Part.position + p.OpenPosition, Vector3.one * 0.3f);
				}
			}
		}
	}

	[System.Serializable]
	public class DoorPart
	{
		public Transform Part;
		public Vector3 OpenPosition;

		[HideInInspector]
		public Vector3 OriginalPosition;
		[HideInInspector]
		public float Time;
		[HideInInspector]
		public bool Opening;
		[HideInInspector]
		public float Speed;
	}
}
