﻿using UnityEngine;
using System.Collections;

public class Collector : MonoBehaviour
{
	public Transform Lift;
	public AudioClip Release, Release2;

	bool busy = false;
	GameObject pack;

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Package>() != null)
		{
			other.transform.parent = Lift;
			other.transform.localPosition = Vector3.zero;
			pack = other.gameObject;
			
			StartCoroutine(LiftAnim());
		}
	}

	IEnumerator LiftAnim()
	{
		busy = true;

		float startTime = Time.time;

		Vector3 pos = Lift.position;
		OneShotSound.Play(Release, Vector3.zero);
		while ((Time.time - startTime) < 1)
		{
			Lift.transform.position = Vector3.Lerp(pos, pos + new Vector3(0, -1, 0), (Time.time - startTime));
			yield return null;
		}
		while ((Time.time - startTime) < 2)
		{
			yield return null;
		}
		Destroy(pack);

		bool playedSound = false;
		while ((Time.time - startTime) < 3)
		{
			Lift.transform.position = Vector3.Lerp(pos + new Vector3(0, -2, 0), pos, (Time.time - startTime - 2));


			if (!playedSound && ((Time.time - startTime) > 2.5f))
			{
				OneShotSound.Play(Release, Vector3.zero);
				playedSound = true;
			}

			yield return null;
		}

		busy = false;
	}
}
